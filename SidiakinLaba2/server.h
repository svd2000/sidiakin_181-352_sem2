#ifndef MYSERVER_H
#define MYSERVER_H

#include <QWidget>
#include "client.h"

class QTcpServer;
class QTextEdit;
class QTcpSocket; //Работа с клиентом


class MyServer : public QWidget {
Q_OBJECT
private:
    QTcpServer* m_ptcpServer; //
    QTcpSocket* pClientSocket;
    QTextEdit*  m_ptxt;
    quint16     m_nNextBlockSize; // аля буфер,отвечающий за правилую доставку сообщения

    QString Invalid;//Статус инвалид (когда клиент нажал на уже использованную клеточку)
    QString Error ;// Статус клиента,что тот не может ничего отправлять
private:
    void sendToClient(QTcpSocket* pSocket, const QString& str); // Отправляет собщение клиенту
    void sendStatus(QTcpSocket* pSocket, const QString& str); // Отправляет статус
     void sendStatusInvalid(QTcpSocket* pSocket, const QString& str);//  Отправляет статус при ошибочном нажатии
    void sendStatusPlayer(QTcpSocket* pClientSocket, const QString& str);
    void sendStatusPlayer2(QTcpSocket* pClientSocket, const QString& str);

public:
    MyServer(int nPort, QWidget* pwgt = 0);// конструктор

    void realization(QString str); // механизм игры
    int checkwin();// проверяет победителя
    void board(); // консольная проверка интерфейса

    char square[10] = { 'o','1','2','3','4','5','6','7','8','9' }; // 9 квадратов

    int player = 1, i, choice;
    int sock = 1;
    char mark;

   QList<QTcpSocket*> sockets;
    QList <QTcpSocket *> _clients;
    QList <QTcpSocket *> _player; // Первый клиент
    QList <QTcpSocket *> _player2; // Второй клиент
    QTcpSocket *firstPlayer; // Сокет первого клиента
    QTcpSocket *secondPlayer;// Сокет второго



public slots:
    virtual void slotNewConnection();// Механизм при подключении
            void slotReadClient   (); // Когда сервер готов читать клиента
};

#endif // MYSERVER_H
