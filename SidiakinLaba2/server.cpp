#include "server.h"
#include "client.h"
#include "QMessageBox"
#include <iostream>
#include <QTcpServer>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QTextCodec>
#include <QLabel>
#include <QTime>
#include <QList>
#include <QCoreApplication>
class QTcpServer;
class QTcpSocket;
class QTextEdit;

MyServer::MyServer(int nPort, QWidget* pwgt) : QWidget(pwgt),
                                                    firstPlayer(NULL), //зануляем для нормальной работы
                                                    secondPlayer(NULL),//аналогично
                                                    m_nNextBlockSize(0)
{
    m_ptcpServer = new QTcpServer(this);// создаем сервер
    if (!m_ptcpServer->listen(QHostAddress::Any, nPort)) {// Записывается адрес ПОРТ клиента
        QMessageBox::critical(this,  "Server Error", "Unable to start the server:" + m_ptcpServer->errorString()); // ошибка если не законектился клиент
        m_ptcpServer->close(); // зыкрывает сервер
        return;
    }

    connect(m_ptcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection())); // Если клиент законнектился,используем данный слот
//интерфейс
    m_ptxt = new QTextEdit;
    m_ptxt->setReadOnly(true); // создаём окно приходящих команд от клиентов
    m_ptxt->setStyleSheet("background: #000; color: #fff;");

    QVBoxLayout* pvbxLayout = new QVBoxLayout;//создаем коробку для интерфейса
    pvbxLayout->addWidget(new QLabel("<H1>Server</H1>")); // заголовок
    pvbxLayout->addWidget(m_ptxt); // поле вывода
    setLayout(pvbxLayout); // выводим все widget-ы
}


void MyServer::slotNewConnection() // функция при подключении
{
    QTcpSocket* pClientSocket = m_ptcpServer->nextPendingConnection(); // срабатывает при новом подключении

    if(!firstPlayer){ // если первое подключение
        connect(pClientSocket, SIGNAL(readyRead()), this, SLOT(slotReadClient())); // если готов читать передаем в функци.
        firstPlayer = pClientSocket; // запихиваем в сокет
        _player << firstPlayer; // и передаём его первому игроку
    }
    else if(!secondPlayer)//аналогично
    {
            connect(pClientSocket, SIGNAL(readyRead()), this, SLOT(slotReadClient()));// готов читать
            secondPlayer = pClientSocket;// запихиваем в массив
            _player2 << secondPlayer;// и передаём его второму игроку
            sendStatusPlayer2(pClientSocket, "error");
    }

    connect(pClientSocket, SIGNAL(disconnected()),pClientSocket, SLOT(deleteLater())); // срабатывает при отключении пользователя и удаляет все с ним связанное командой deleteLater
    sendToClient(pClientSocket, "Server Response: Connected!"); // отправляет сообщение о соединении
}



void MyServer::slotReadClient()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender(); // вызывает определённого клиента (Который сейчас ходит)

    QDataStream in(pClientSocket); //считываем сокет клиента
    in.setVersion(QDataStream::Qt_4_2); // Версия датастримы


    // базовая проверка (Для того чтобы можно было читать сообщение блоками)
    for (;;)
    {
        if (!m_nNextBlockSize) // если бфер пустой
        {

            if (pClientSocket->bytesAvailable() < sizeof(quint16)) //
            {
                break;
            }

            in >> m_nNextBlockSize; // считывает блок
        }



        if (pClientSocket->bytesAvailable() < m_nNextBlockSize)
        {
            break;

        }
     ///////////////////////////////////////////////////////////////////////////


        sock = (sock % 2) ? 1 : 2; // если чётный то ходит второй игрок ( 0 ), не чётный ходит ( Х ).

        QString str;
        in >> str; // считывается сообщение


        QString strMessage = "Client has sende: " + str; // клиент отправил сообщение и текст отправления
        m_ptxt->append(strMessage);

        Invalid = ""; //зануляем по умочанию чтоб все ходы не были инвалдиными
        Error = ""; // аналогично

        realization(str); // здесь происходит все, что связзанно с механизмом игры

        QByteArray arr =  pClientSocket->readAll(); // считывает информацию в байтовый массив



        if(Invalid != "Invalid move"){ // если нет ошибок
            // отправка сообщений пользователям 1 и 2-у
                foreach(pClientSocket, _player) { // пишем входящие данные от "вещающего" получателям
                    if (pClientSocket->state() == QTcpSocket::ConnectedState)
                        pClientSocket->write(arr);
/////////////////////////////////////////////////////////////////////////

                    if(sock == 2) // если ходил х то ход передаётся 0
                    {
                        sendStatusPlayer(pClientSocket, "GO:");// отправляет статус о том что можно ходить следуйщему
                    }

                     sendToClient(pClientSocket, "You wrote: " + arr); // сообщение
                }



                foreach(pClientSocket, _player2) { // пишем входящие данные от "вещающего" получателям
                    if (pClientSocket->state() == QTcpSocket::ConnectedState)
                        pClientSocket->write(arr);


                    if (sock == 1)// если ходил 0 то ход передаётся х
                    {
                        sendStatusPlayer(pClientSocket, "GO:"); // отправляет статус о том что можно ходить следуйщему
                    }

                     sendToClient(pClientSocket, "You wrote: " + arr);// сообщение
                }


          }


            sock++; // для смены игрока


        m_nNextBlockSize = 0;//зануляем буфер
    }
}

void MyServer::sendStatusPlayer(QTcpSocket* pClientSocket, const QString& str){
    QByteArray  arrBlock;
// базовая проверка
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_10);
    out << quint16(0) << str;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));
//******************************************************************

    if(sock == 2){ // отправка статуса
        foreach(pClientSocket, _player) { // пишем входящие данные от "вещающего" получателям
            if (pClientSocket->state() == QTcpSocket::ConnectedState)
                pClientSocket->write(arrBlock);// отправка нужному игроку

        }
    }

    if (sock == 1)// отправка статуса
    {
        foreach(pClientSocket, _player2) { // пишем входящие данные от "вещающего" получателям
            if (pClientSocket->state() == QTcpSocket::ConnectedState)
                pClientSocket->write(arrBlock);// отправка нужному игроку

        }
    }

}
///////////////////////////////////////////////
void MyServer::sendToClient(QTcpSocket* pSocket, const QString& str)
{

    // дальше просто рисуем поле игры
    QByteArray  arrBlock;
   QString DataAsString = QTextCodec::codecForMib(1015)->toUnicode(arrBlock);
    qDebug() << arrBlock;
    DataAsString = arrBlock.append("  ");arrBlock.append(square[1]);arrBlock.append("     |   "); arrBlock.append(square[2]); arrBlock.append("    |     "); arrBlock.append(square[3]);
    DataAsString = arrBlock.append("\n_____|_____|_____\n");
    DataAsString = arrBlock.append("  ");arrBlock.append(square[4]);arrBlock.append("     |   "); arrBlock.append(square[5]); arrBlock.append("    |     "); arrBlock.append(square[6]);
    DataAsString = arrBlock.append("\n_____|_____|_____\n");
    DataAsString = arrBlock.append("  ");arrBlock.append(square[7]);arrBlock.append("     |   "); arrBlock.append(square[8]); arrBlock.append("    |     "); arrBlock.append(square[9]);
    DataAsString = arrBlock.append("\n");

// базовая проверка
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_10);
    out << quint16(0) << DataAsString;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));
//****************************************************

    pSocket->write(arrBlock);// отправка графики клеток и введённых данных
}

void MyServer::sendStatus(QTcpSocket* pClientSocket, const QString& str){
// базовая проверка
    QByteArray  arrBlock;

    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_10);
    out << quint16(0) << str;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));
//****************************************************


        foreach(pClientSocket, _player) { // пишем входящие данные от "вещающего" получателям
            if (pClientSocket->state() == QTcpSocket::ConnectedState)
                pClientSocket->write(arrBlock);// отправка статуса

         }

        foreach(pClientSocket, _player2) { // пишем входящие данные от "вещающего" получателям
            if (pClientSocket->state() == QTcpSocket::ConnectedState)
                pClientSocket->write(arrBlock);// отправка статуса

         }


}

void MyServer::sendStatusInvalid(QTcpSocket* pClientSocket, const QString& str){
    QByteArray  arrBlock;
// базовая проверка
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_10);
    out << quint16(0) << str;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));
//****************************************************



            foreach(pClientSocket, _player) { // пишем входящие данные от "вещающего" получателям
                if (pClientSocket->state() == QTcpSocket::ConnectedState)
                    pClientSocket->write(arrBlock);// отправка статуса о не правильном ходе

            }



            foreach(pClientSocket, _player2) { // пишем входящие данные от "вещающего" получателям
                if (pClientSocket->state() == QTcpSocket::ConnectedState)
                    pClientSocket->write(arrBlock);// отправка статуса о не правильном ходе


            }


    QByteArray  arrError;

    Error = "error";


    QDataStream outE(&arrError, QIODevice::WriteOnly);
    outE.setVersion(QDataStream::Qt_5_10);
    outE << quint16(0) << Error;

    outE.device()->seek(0);
    outE << quint16(arrError.size() - sizeof(quint16));

    // отправляем статус error
        if(sock == 2){
            foreach(pClientSocket, _player) { // пишем входящие данные от "вещающего" получателям
                if (pClientSocket->state() == QTcpSocket::ConnectedState)
                    pClientSocket->write(arrError); // отправка статуса о не правильном ходе

            }
        }

        if(sock == 1){
            foreach(pClientSocket, _player2) { // пишем входящие данные от "вещающего" получателям
                if (pClientSocket->state() == QTcpSocket::ConnectedState)
                    pClientSocket->write(arrError);// отправка статуса о не правильном ходе

            }
        }


}


void MyServer::realization(QString str)// функционирование крестиков ноликов
{
    Invalid = "";
    board();
    player = (player % 2) ? 1 : 2; // проверяет чей ход
    std::cout << "Player " << player << ", enter a number:  "; // Оповещение о том кто ходит в даный момент
    /*std::cin >> choice;*/ // запись выбранного квадрата

    choice = str.toInt();

    mark = (player == 1) ? 'X' : 'O'; // либо Х либо О
    if (choice == 1 && square[1] == '1') // если выбран 1-й квадрат то в него записывается Х или О и т.д.....
            square[1] = mark;
    else if (choice == 2 && square[2] == '2')
            square[2] = mark;
    else if (choice == 3 && square[3] == '3')
            square[3] = mark;
    else if (choice == 4 && square[4] == '4')
            square[4] = mark;
    else if (choice == 5 && square[5] == '5')
            square[5] = mark;
    else if (choice == 6 && square[6] == '6')
            square[6] = mark;
    else if (choice == 7 && square[7] == '7')
            square[7] = mark;
    else if (choice == 8 && square[8] == '8')
            square[8] = mark;
    else if (choice == 9 && square[9] == '9') // если выбран 9-й квадрат то в него записывается Х или О
            square[9] = mark;
    else
    {
            std::cout << "\t\t\t\t\t\t   Invalid move "; // если повторно выбран один из квадратов то стабатывает данный кусок
            m_ptxt->append("Invalid move");
            Invalid = "Invalid move";
            sendStatusInvalid(pClientSocket, Invalid);// отправка статуса о не правильном ходе
            player--; // делаем так чтобы снова мог походить тотже игрок
             sock--;// делаем так чтобы снова мог походить тотже игрок

    }

    i = checkwin();// проверка на победителя
    player++; // новый игрок


    board();// консольная проверка хода игры
    if (i == 1){ // если случается это значит кто-то победил
             QTcpSocket* pClientSocket = (QTcpSocket*)sender();// отслеживаем последнего игрока
            std::cout << "\t\t\t\t\t\t   " << "Player " << --player << " win ";
            QString WhoPlayer = QString::number(+player);

            if(WhoPlayer == "2"){ // проверяем кто победил и выводим информацию всем игрокам
                 m_ptxt->append("WIN O");
                 QString OWin = "WIN O";
                  m_ptxt->append("Invalid move");
 sendStatusPlayer(pClientSocket, "Invalid move");
                 sendStatus(pClientSocket, OWin); // победтл 0
            }else {
                 m_ptxt->append("WIN X");
                 QString XWin = "WIN X";
                 m_ptxt->append("Invalid move");
 sendStatusPlayer(pClientSocket, "Invalid move");
                 sendStatus(pClientSocket, XWin);// победтл х
            }


    }
    else if(i == 0){// игра зашла в тупик и статус переходит в ничью
            std::cout << "\t\t\t\t\t\t   " << "Game draw";
            m_ptxt->append("Draw");
            QString Draw = "Draw";
            sendStatus(pClientSocket, Draw); // отправка статуса
    }

}



int MyServer::checkwin()
{
    // проверка выйгрыша (Здсь описаны ве возможные выйграши). Если что-то из этого сходится то определяется победитель иначе игра продолжается
        if (square[1] == square[2] && square[2] == square[3])
                return 1;
        else if (square[4] == square[5] && square[5] == square[6])
                return 1;
        else if (square[7] == square[8] && square[8] == square[9])
                return 1;
        else if (square[1] == square[4] && square[4] == square[7])
                return 1;
        else if (square[2] == square[5] && square[5] == square[8])
                return 1;
        else if (square[3] == square[6] && square[6] == square[9])
                return 1;
        else if (square[1] == square[5] && square[5] == square[9])
                return 1;
        else if (square[3] == square[5] && square[5] == square[7])
                return 1;
        else if (square[1] != '1' && square[2] != '2' && square[3] != '3' && square[4] != '4' && square[5] != '5' && square[6] != '6' && square[7] != '7' && square[8] != '8' && square[9] != '9')
                return 0; // заполненны все клетка и победителя нет

        else
                return -1; // продолжение игры
}


/******************************************************************* ФУНКЦИЯ ВИЗУАЛИЗАЦИИ ********************************************************************/


void MyServer::board() // по сути дело не нужная таблица (Использется для проверки в консоли)
{
        system("cls");
        std::cout << "\t\t\t\t\t\t" <<"     |     |     " << std::endl;
        std::cout << "\t\t\t\t\t\t" << "  " << square[1] << "  |  " << square[2] << "  |  " << square[3] << std::endl;
        std::cout << "\t\t\t\t\t\t" << "_____|_____|_____" << std::endl;
        std::cout << "\t\t\t\t\t\t" << "  " << square[4] << "  |  " << square[5] << "  |  " << square[6] << std::endl;
        std::cout << "\t\t\t\t\t\t" << "_____|_____|_____" << std::endl;
        std::cout << "\t\t\t\t\t\t" << "     |     |     " << std::endl;
        std::cout << "\t\t\t\t\t\t" << "  " << square[7] << "  |  " << square[8] << "  |  " << square[9] << std::endl;
        std::cout << "\t\t\t\t\t\t" << "     |     |     " << std::endl << std::endl;


}
void MyServer::sendStatusPlayer2(QTcpSocket* pClientSocket, const QString& str){ // отправляет статус от дефолтном блоке второго клиента
QByteArray arrBlock;
// базовая проверка
QDataStream out(&arrBlock, QIODevice::WriteOnly);
out.setVersion(QDataStream::Qt_5_10);
out << quint16(0) << str;

out.device()->seek(0);
out << quint16(arrBlock.size() - sizeof(quint16));
//******************************************************************

if (sock == 1)// отправка статуса
{
foreach(pClientSocket, _player2) { // пишем входящие данные от "вещающего" получателям
if (pClientSocket->state() == QTcpSocket::ConnectedState)
pClientSocket->write(arrBlock);// отправка нужному игроку

}
}

}
