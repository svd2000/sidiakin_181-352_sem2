/********************************************************************************
** Form generated from reading UI file 'delluserdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DELLUSERDIALOG_H
#define UI_DELLUSERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_DellUserDialog
{
public:
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QLabel *label;

    void setupUi(QDialog *DellUserDialog)
    {
        if (DellUserDialog->objectName().isEmpty())
            DellUserDialog->setObjectName(QString::fromUtf8("DellUserDialog"));
        DellUserDialog->resize(400, 300);
        lineEdit = new QLineEdit(DellUserDialog);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(20, 20, 113, 20));
        pushButton = new QPushButton(DellUserDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(40, 50, 75, 23));
        label = new QLabel(DellUserDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(150, 20, 161, 16));

        retranslateUi(DellUserDialog);

        QMetaObject::connectSlotsByName(DellUserDialog);
    } // setupUi

    void retranslateUi(QDialog *DellUserDialog)
    {
        DellUserDialog->setWindowTitle(QApplication::translate("DellUserDialog", "Dialog", nullptr));
        pushButton->setText(QApplication::translate("DellUserDialog", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", nullptr));
        label->setText(QApplication::translate("DellUserDialog", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\273\320\276\320\263\320\270\320\275 \320\264\320\273\321\217 \321\203\320\264\320\260\320\273\320\265\320\275\320\270\321\217", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DellUserDialog: public Ui_DellUserDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DELLUSERDIALOG_H
