#ifndef DEATH_H
#define DEATH_H

#include <QDialog>

namespace Ui {
class Death;
}

class Death : public QDialog
{
    Q_OBJECT

public:
    explicit Death(QWidget *parent = nullptr);
    ~Death();

private slots:


    void on_pic1_clicked();

    void on_pic2_clicked();

private:
    Ui::Death *ui;
};

#endif // DEATH_H
