#ifndef SECOND_H
#define SECOND_H

#include <QDialog>

namespace Ui {
class Second;
}

class Second : public QDialog
{
    Q_OBJECT

public:
    explicit Second(QWidget *parent = nullptr);
    ~Second();

private slots:
    void on_pushButton112_clicked();

    void on_write_clicked();

    void on_lineEditsec_cursorPositionChanged(int arg1, int arg2);

private:
    Ui::Second *ui;
};

#endif // SECOND_H
