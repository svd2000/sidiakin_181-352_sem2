#include "meneger.h"
#include "ui_meneger.h"
#include <iostream>
#include "QSqlQuery"
#include "QtSql/QSqlDatabase"
#include "QProcess"
Meneger::Meneger(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Meneger)
{
    ui->setupUi(this);
}

Meneger::~Meneger()
{
    delete ui;
}

void Meneger::on_managerButton_clicked()
{
    QSqlDatabase db;
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("C:\\Users\\svd20\\Desktop\\qw\\database.db3");
    db.open();

    //Осуществляем запрос
    QSqlQuery query;
    query.exec("SELECT name, price, gang, executor FROM People");

    //Выводим значения из запроса
    while (query.next())
    {
    QString name = query.value(0).toString();
    QString price = query.value(1).toString();
    QString gang = query.value(2).toString();
    QString executor = query.value(3).toString();
    ui->managerEdit->insertPlainText(name+" "+price+" "+gang+" "+executor+"\n");
    }
}
