#ifndef MENEGER_H
#define MENEGER_H

#include <QDialog>

namespace Ui {
class Meneger;
}

class Meneger : public QDialog
{
    Q_OBJECT

public:
    explicit Meneger(QWidget *parent = nullptr);
    ~Meneger();

private slots:
    void on_managerButton_clicked();

private:
    Ui::Meneger *ui;
};

#endif // MENEGER_H
