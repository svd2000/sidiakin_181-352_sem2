#include "mainwindow.h"
#include <QApplication>
#include "authdialog.h"
#include "users.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
