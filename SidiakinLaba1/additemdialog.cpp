#include "additemdialog.h"
#include "ui_additemdialog.h"

AddItemDialog::AddItemDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddItemDialog)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL (clicked()), this, SLOT (on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL (clicked()), this, SLOT (on_pushButton_2_clicked()));
}

AddItemDialog::~AddItemDialog()
{
    delete ui;
}

shopItem AddItemDialog::GetInputData()
{
    return inputItem;
}

void AddItemDialog::on_pushButton_clicked()
{
    inputItem.Setcustomer(ui->lineEdit->text());
    inputItem.Setprice(ui->lineEdit_2->text());
    inputItem.Setlogbook(ui->lineEdit_3->text());
    inputItem.Setgang(ui->lineEdit_4->text());

    this->accept();
}

void AddItemDialog::on_pushButton_2_clicked()
{
    this->reject();
}
