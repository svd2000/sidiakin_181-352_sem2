#ifndef shopITEM_H
#define shopITEM_H

#include <QString>

class shopItem
{
public:
    shopItem();
    shopItem(QString customer, QString price, QString logbook, QString gang);

    QString Getcustomer() const;
    QString Getprice() const;
    QString Getlogbook() const;
    QString Getgang() const;

    void Setcustomer(QString customer);
    void Setprice(QString price);
    void Setlogbook(QString logbook);
    void Setgang(QString gang);

private:
    QString customer;
    QString price;
    QString logbook;
    QString gang;
};

#endif // shopITEM_H
