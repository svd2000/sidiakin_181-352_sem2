#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "authdialog.h"
#include "additemdialog.h"
#include "adduserdialog.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)

{
    AuthDialog dlg(this);

    usersDB.SetFilePath("D:/QT this/SidiakinLaba1/users.json"); // Задаем путь к файлу БД пользователей
    usersDB.Load();

    bool authComplete = false;

    while (!authComplete) { // Выполняем цикл, пока пользователь не прошел авторизацию
        dlg.exec();

        QString login = dlg.GetInputData().GetLogin();
        QString pass = dlg.GetInputData().GetPassword();

        if (usersDB.FindUser(login, activeUser) == true) {
            if (activeUser.GetPassword() == pass)
                authComplete = true;
            else { // если не совпадает пароль, то выводим ошибку
                QMessageBox msg;
                msg.setText("Неверный пароль!");
                msg.exec();
            }
        }
        else { // если не найден пользователь, то выводим ошибку
            QMessageBox msg;
            msg.setText("Пользователь с таким именем не найден!");
            msg.exec();
        }
    }

    ui->setupUi(this); // Инициализация интерфейса

    connect(ui->pushButton, SIGNAL (clicked()), this, SLOT (on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL (clicked()), this, SLOT (on_pushButton_2_clicked()));
    connect(ui->pushButton_5, SIGNAL (clicked()), this, SLOT (on_pushButton_5_clicked()));

    ui->label->setText("Ваш логин: " + activeUser.GetLogin());
    QString permissionText; // Определяем права пользователя
    switch (activeUser.GetPermission()) {
        case 2:
            permissionText = "Администратор";
        break;
        case 1:
            permissionText = "Редактирование";
        break;
        case 0:
            permissionText = "Только просмотр";
        break;
    }
    ui->label_2->setText("Ваш уровень доступа: " + permissionText);

    shopDB.SetFilePath("D:/QT this/SidiakinLaba1/data.json");
    shopDB.Load();

    QList<shopItem> items = shopDB.GetItems();
    for (auto item : items) {
        ui->tableWidget->insertRow (ui->tableWidget->rowCount());
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 0, new QTableWidgetItem(item.Getcustomer()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 1, new QTableWidgetItem(item.Getprice()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 2, new QTableWidgetItem(item.Getlogbook()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 3, new QTableWidgetItem(item.Getgang()));

    }

    for (int c = 0; c < ui->tableWidget->horizontalHeader()->count(); ++c)
    {
        ui->tableWidget->horizontalHeader()->setSectionResizeMode(
            c, QHeaderView::Stretch);
    }

    if (activeUser.GetPermission() == 0) { // Если права не позволяют редактировать данные
        ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers); // То ограничиваем редактирование таблицы
        ui->pushButton_2->hide();
        ui->pushButton_3->hide();
        ui->pushButton_4->hide();
    } else if (activeUser.GetPermission() == 1) {
        ui->pushButton_3->hide();
        ui->pushButton_4->hide();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    // Меняем пароль при нажатии на кнопку
    if (usersDB.ChangePassword(activeUser.GetLogin(), ui->lineEdit->text()) == true) {
        usersDB.Save();
        QMessageBox msg;
        msg.setText("Пароль изменен!");
        msg.exec();
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    for (int i = 0; i < ui->tableWidget->verticalHeader()->count(); i++) {
        shopItem editedItem(ui->tableWidget->item(i, 0)->text(), ui->tableWidget->item(i, 1)->text(), ui->tableWidget->item(i, 2)->text(), ui->tableWidget->item(i, 3)->text());
        shopDB.UpdateItem(i, editedItem);
    }

    shopDB.Save();
}

void MainWindow::on_pushButton_3_clicked()
{
    AddItemDialog dlg(this);
    if (dlg.exec() == QDialog::Accepted) {
        ui->tableWidget->insertRow (ui->tableWidget->rowCount());
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 0, new QTableWidgetItem(dlg.GetInputData().Getcustomer()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 1, new QTableWidgetItem(dlg.GetInputData().Getprice()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 2, new QTableWidgetItem(dlg.GetInputData().Getlogbook()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 3, new QTableWidgetItem(dlg.GetInputData().Getgang()));
        shopDB.AddItem(dlg.GetInputData());
    }
}

void MainWindow::on_pushButton_4_clicked()
{
    AddUserDialog dlg(this);
    if (dlg.exec() == QDialog::Accepted) {
        usersDB.AddUser(dlg.GetInputData());
        QMessageBox msg;
        msg.setText("Пользователь добавлен!");
        msg.exec();
        usersDB.Save();
    }
}

void MainWindow::on_pushButton_5_clicked()
{

for (int i = 1; i < ui->tableWidget->verticalHeader()->count(); i++) {
ui->tableWidget->showRow(i);
}

for (int i = 0; i < ui->tableWidget->verticalHeader()->count(); i++) {
    if (!ui->tableWidget->item(i, 1)->text().contains(ui->lineEdit_2->text())) {
        ui->tableWidget->hideRow(i);
    }
}
}
