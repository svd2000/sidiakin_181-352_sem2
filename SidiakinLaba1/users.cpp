#include "users.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>

Users::Users()
{

}

Users::Users(QString filePath)
{
    this->filePath = filePath;
    Load();
}

void Users::Load()
{

    QString val; // Содержимое файла
    QFile file;
    file.setFileName(filePath);

    if (file.open(QIODevice::ReadOnly | QIODevice::Text) == true) {
        val = file.readAll();
        file.close();
    } else { // Если не удалось открыть файл, то выводим сообщение о ошибке
        QMessageBox msg;
        msg.setText("Не удалось прочитать файл с базой пользователей!");
        msg.exec();
        exit(EXIT_FAILURE);
    }

    QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8());
    QJsonObject obj = doc.object();
    QJsonArray jUsers = obj.take("users").toArray();

    for (auto jUser : jUsers) {
        QString login, password; int permission;

        login = jUser.toObject().take("login").toString();
        password = jUser.toObject().take("password").toString();
        permission = jUser.toObject().take("permission").toInt();

        UserData user(login, password, permission);

        db.push_back(user);
    }
}

void Users::Save()
{
    QFile file;
    file.setFileName(filePath);

    QJsonDocument doc;
    QJsonObject obj = doc.object();
    QJsonArray arr;

    for (auto User : db) {
        QJsonObject jUser;

        jUser.insert("login", User.GetLogin());
        jUser.insert("password", User.GetPassword());
        jUser.insert("permission", User.GetPermission());

        arr.push_back(jUser);
    }

    obj.insert("users", arr);
    doc.setObject(obj);

    if (file.open(QIODevice::WriteOnly | QIODevice::Text) == true) {
        file.write(doc.toJson());
        file.close();
    } else {
        QMessageBox msg;
        msg.setText("Не удалось записать файл с базой пользователей!");
        msg.exec();
        exit(EXIT_FAILURE);
    }
}

bool Users::FindUser(QString login, UserData &user)
{
    for (auto User : db) {
        if (User.GetLogin() == login) {
            user = User;
            return true;
        }
    }

    return false;
}

bool Users::ChangePassword(QString login, QString newPassword)
{
    for (int i = 0; i < db.size(); i++) {
        if (db[i].GetLogin() == login) {
            db[i].SetPassword(newPassword);
            return true;
        }
    }
    return false;
}

void Users::SetFilePath(QString filePath)
{
    this->filePath = filePath;
}

void Users::AddUser(UserData userData)
{
    db.push_back(userData);
}
