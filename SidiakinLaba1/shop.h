#ifndef shop_H
#define shop_H

#include <QString>
#include <QList>
#include <shopitem.h>

class shop
{
public:
    shop();

    void SetFilePath(QString filePath);
    bool UpdateItem(int index, shopItem editedItem);
    void AddItem(shopItem item);

    void Load();
    void Save();

    QList<shopItem> GetItems() const;

private:
    QList<shopItem> db;
    QString filePath;
};

#endif // shop_H
