#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "users.h"
#include "shop.h"
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::MainWindow *ui;
    Users usersDB;
    shop shopDB;
    UserData activeUser;
};

#endif // MAINWINDOW_H
