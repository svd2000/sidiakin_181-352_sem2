#include "userdata.h"

UserData::UserData()
{

}

UserData::UserData(QString log, QString pass, int perm)
{
    this->login = log;
    this->password = pass;
    this->permission = perm;
}

QString UserData::GetLogin() const
{
    return login;
}

QString UserData::GetPassword() const
{
    return password;
}

int UserData::GetPermission() const
{
    return permission;
}

void UserData::SetLogin(QString login)
{
    this->login = login;
}

void UserData::SetPassword(QString pass)
{
    this->password = pass;
}

void UserData::SetPermission(int permission)
{
    this->permission = permission;
}

