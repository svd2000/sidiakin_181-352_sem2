
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>
#include "shop.h"

shop::shop()
{

}

void shop::SetFilePath(QString filePath)
{
    this->filePath = filePath;
}

bool shop::UpdateItem(int index, shopItem editedItem)
{
    if (index > db.count() - 1) return false;
    db[index] = editedItem;
    return true;
}

void shop::AddItem(shopItem item)
{
    db.push_back(item);
}

void shop::Load()
{

    QString val;
    QFile file;
    file.setFileName(filePath);

    if (file.open(QIODevice::ReadOnly | QIODevice::Text) == true) {
        val = file.readAll();
        file.close();
    } else {
        QMessageBox msg;
        msg.setText("Не удалось прочитать файл с");
        msg.exec();
        exit(EXIT_FAILURE);
    }

    QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8());
    QJsonObject obj = doc.object();
    QJsonArray jItems = obj.take("items").toArray();

    for (auto jItem : jItems) {
        QString customer, price, logbook, gang;

        customer = jItem.toObject().take("customer").toString();
        price = jItem.toObject().take("price").toString();
        logbook = jItem.toObject().take("logbook").toString();
        gang = jItem.toObject().take("gang").toString();


        shopItem item(customer, price, logbook, gang);

        db.push_back(item);
    }
}

void shop::Save()
{
    QFile file;
    file.setFileName(filePath);

    QJsonDocument doc;
    QJsonObject obj = doc.object();
    QJsonArray arr;

    for (auto Item : db) {
        QJsonObject jItem;

        jItem.insert("logbook", Item.Getlogbook());
        jItem.insert("price", Item.Getprice());
        jItem.insert("customer", Item.Getcustomer());
        jItem.insert("gang", Item.Getgang());

        arr.push_back(jItem);
    }

    obj.insert("items", arr);
    doc.setObject(obj);

    if (file.open(QIODevice::WriteOnly | QIODevice::Text) == true) {
        file.write(doc.toJson());
        file.close();
    } else {
        QMessageBox msg;
        msg.setText("Не удалось записать файл с базой!");
        msg.exec();
        exit(EXIT_FAILURE);
    }
}

QList<shopItem> shop::GetItems() const
{
    return db;
}
