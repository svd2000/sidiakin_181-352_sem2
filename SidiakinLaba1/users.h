#ifndef USERS_H
#define USERS_H

#include <QString>
#include <QList>
#include "userdata.h"

class Users // БД пользователей
{
public:
    Users();
    Users(QString filePath); // Развернуть базу данных из файла

    void Load();
    void Save();

    bool FindUser(QString login, UserData &user);
    bool ChangePassword(QString login, QString newPassword);

    void SetFilePath(QString filePath);
    void AddUser(UserData userData);

private:
    QString filePath;
    QList<UserData> db;
};

#endif // USERS_H
