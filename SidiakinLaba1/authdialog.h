#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include <QDialog>
#include <userdata.h>

namespace Ui {
class AuthDialog;
}

class AuthDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AuthDialog(QWidget *parent = nullptr);
    ~AuthDialog();

    UserData GetInputData();

private slots:
    void on_pushButton_clicked();

    void on_AuthDialog_rejected();

private:
    UserData user;

    Ui::AuthDialog *ui;
};

#endif // AUTHDIALOG_H
