#include "shopitem.h"

shopItem::shopItem()
{

}

shopItem::shopItem(QString customer, QString price, QString reciver, QString gang)
{
    this->customer = customer;
    this->price = price;
    this->logbook = reciver;
    this->gang = gang;
}

QString shopItem::Getcustomer() const
{
    return customer;
}

QString shopItem::Getprice() const
{
    return price;
}

QString shopItem::Getlogbook() const
{
    return logbook;
}

QString shopItem::Getgang() const
{
    return gang;
}

void shopItem::Setcustomer(QString customer)
{
    this->customer = customer;
}

void shopItem::Setprice(QString price)
{
    this->price = price;
}

void shopItem::Setlogbook(QString logbook)
{
    this->logbook = logbook;
}

void shopItem::Setgang(QString gang)
{
    this->gang = gang;
}
