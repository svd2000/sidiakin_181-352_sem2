#include "adduserdialog.h"
#include "ui_adduserdialog.h"

AddUserDialog::AddUserDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddUserDialog)
{
    ui->setupUi(this);
}

AddUserDialog::~AddUserDialog()
{
    delete ui;
}

void AddUserDialog::on_pushButton_clicked()
{
    user.SetLogin(ui->lineEdit->text());
    user.SetPassword(ui->lineEdit_2->text());
    user.SetPermission(ui->comboBox->currentIndex());
    this->accept();
}

void AddUserDialog::on_pushButton_2_clicked()
{
    this->reject();
}

UserData AddUserDialog::GetInputData()
{
    return user;
}
